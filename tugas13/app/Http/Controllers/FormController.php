<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function bio(){
        return view('halaman.form');
    }

    public function kirim(Request $request){
        $fnama = $request['fnama'];
        $nama = $request['nama'];
        return view('halaman.home', compact('fnama','nama'));
    }
}
